import re
import sys

# EDIT THIS REGEXP
REGEXP="REGEXP"


# DO NOT CHANGE ANYTHING BELOW THIS LINE

def validate(value, regexp):
	"""
	Function to validate a string stored in variable value given
	a regular expression in regexp.
	"""

	val = re.compile(regexp)
	result = val.match(value)

	if result and value == result.group(0):
		return True

	return False 



def main():
	try:
		fd = open(sys.argv[1])
		lines = fd.readlines()
	except:
		print "Could not open strings file."
		print "Usage: python %s <strings_file>" % sys.argv[0]
		sys.exit(0)

	passed = 0
	failed = 0

	for line in lines:
		line = line.strip()

		if validate(line, REGEXP):
			print line, "PASS"
			passed+=1
		else:
			print line, "FAIL"
			failed+=1


	print "%s strings passed, %s strings failed" % (passed, failed)

main()
